@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

external open class Filter(vertexSrc: String?, fragmentSrc: String?) {
    val uniforms: dynamic
}