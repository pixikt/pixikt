@file:JsModule("pixi.js")
@file:JsNonModule

package PIXI

import kotlin.js.Json

external open class TextStyle(style: Json) : DisplayObject