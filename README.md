# Pixi.kt

Incomplete [Kotlin](https://kotlinlang.org/) interface for [PixiJS](http://www.pixijs.com/) 4.6.2 for porting the [PixiJS examples](http://pixijs.io/examples/) to Kotlin.

see also https://gitlab.com/pixikt/pixikt.gitlab.io